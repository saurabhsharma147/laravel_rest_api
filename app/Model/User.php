<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable {

    use SoftDeletes;

    protected $table = 'user';
    protected $primaryKey = 'user_id';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at','deleted_at'
    ];

   public function getUser($userid = 0, $email = null, $role_type = null) {

        $user = User::select('user_id','password','email');
        if ($userid) {
            $user->where($this->table . '.user_id', '=', $userid);
        } else if ($email) {
            $user->where('email', '=', $email)
                    ->where('role_type', '=', $role_type);
        }
        return $user->first();
    }

}
