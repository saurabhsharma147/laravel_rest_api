<?php

namespace App\Http\Requests;

class RegisterRequest extends BaseApiRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:user',
            'password' => 'required|min:8|max:25',
            'role_type' => 'required|in:2,3',
            'device_id' => 'required',
            'device_type' => 'required|in:1,2',
            'device_token' => 'sometimes'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
