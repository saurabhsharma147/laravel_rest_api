<?php

return [
    'invalid_login_credentials' => 'These credentials do not match our records.',
    'login_success' => 'User was logged in successfully.',

];