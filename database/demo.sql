-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2016 at 08:46 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_user_tokens`
--

CREATE TABLE IF NOT EXISTS `app_user_tokens` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) NOT NULL,
  `device_id` varchar(64) DEFAULT NULL,
  `device_type` tinyint(4) NOT NULL COMMENT '1=>iOS,2=>Android',
  `device_token` char(255) NOT NULL,
  `access_token` char(64) NOT NULL,
  `device_os` varchar(50) DEFAULT NULL,
  `device_model` varchar(50) DEFAULT NULL,
  `device_os_version` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_user_tokens`
--

INSERT INTO `app_user_tokens` (`id`, `user_id`, `device_id`, `device_type`, `device_token`, `access_token`, `device_os`, `device_model`, `device_os_version`, `created_at`, `updated_at`, `deleted_at`) VALUES
(267, 207, '2asfasfjg01', 1, '', 'e2b67bd51456f1ac06a900a188aca485', '', '', '', '2016-09-02 18:36:05', '2016-09-02 18:36:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `role_type` tinyint(1) NOT NULL DEFAULT '2' COMMENT 'admin=>1, user=>2, coder=>3',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - registration incomplete,1 - registration completed,2 - user suspended',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` date DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `password`, `remember_token`, `role_type`, `status`, `created_at`, `updated_at`, `deleted_at`, `is_approved`) VALUES
(207, 'Saurabh', 'info@coding4developers.com', '$2y$10$uZFu269ZfQqjkEUqQUif7.HK3vcr2egOP7JdWa3dJ7qnkzCwSLlkm', '', 2, 0, '2016-09-02 13:06:05', '2016-09-02 13:06:05', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_user_tokens`
--
ALTER TABLE `app_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `USER_DEVICE_idx` (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_user_tokens`
--
ALTER TABLE `app_user_tokens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=268;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=208;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `app_user_tokens`
--
ALTER TABLE `app_user_tokens`
  ADD CONSTRAINT `app_user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
