<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AppUserToken extends Model
{
  protected $table  = 'app_user_tokens';


  public function saveDeviceDetails($device_data,$user_id){

    $this->user_id = $user_id;
    $this->device_id = $device_data['device_id'];
    $this->device_token =  isset($device_data['device_token'])?$device_data['device_token']:'';
    $this->device_os = isset($device_data['device_os'])?$device_data['device_os']:'';
    $this->device_type = $device_data['device_type'];
    $this->device_model = isset($device_data['device_model'])?$device_data['device_model']:'';
    $this->device_os_version = isset($device_data['device_os_version'])?$device_data['device_os_version']:'';
    $this->access_token = md5(uniqid(mt_rand(), true));
    $this->save();
   
  } 

}
