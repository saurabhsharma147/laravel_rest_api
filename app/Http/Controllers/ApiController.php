<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Model\User;
use App\Model\AppUserToken;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ApiController extends Controller {

    protected static $data = [
        'message' => '',
        'success' => false,
        'errors' => [],
        'status_code' => \Symfony\Component\HttpFoundation\Response::HTTP_OK
    ];

    public function __construct() {
        
    }

    public function postRegister(\App\Http\Requests\RegisterRequest $request) {
        $data = Request::all();
        try {
            if (!empty($data)) {
                $user = new User();
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->role_type = $data['role_type'];
                $user->password = \Hash::make($data['password']);
                if ($user->save()) {
                    self::$data = self::loginUser($data);
                }
            }
        } catch (\Exception $e) {
            Log::error("ApiController::postRegister() there is some exception " . $e->getMessage());
            self::$data['status_code'] = \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR;
            self::$data['message'] = trans('messages.exception_msg');
        }
        return self::$data;
    }

    public function loginUser($data) {
        try {
            $user_obj = new User();
            $user = $user_obj->getUser(0, $data['email'], $data['role_type']);
            if ($user and \Hash::check($data['password'], $user->password)) {
                $app_user_token = AppUserToken::where('user_id', '=', $user->user_id)
                                ->where('device_type', '=', $data['device_type'])->first();
                if (!$app_user_token) {
                    $app_user_token = new AppUserToken();
                    $app_user_token->saveDeviceDetails($data, $user->user_id);
                    self::$data['user'] = $user;
                    self::$data['access_token'] = $app_user_token->access_token;
                    self::$data['success'] = true;
                    self::$data['message'] = trans('messages.login_success');
                }
            } else {
                self::$data['success'] = false;
                self::$data['message'] = trans('messages.invalid_login_credentials');
            }
        } catch (\Exception $e) {
            Log::error("ApiController::loginUser() there is some exception " . $e->getMessage());
            self::$data['status_code'] = \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR;
            self::$data['message'] = trans('messages.exception_msg');
        }
        return self::$data;
    }

////
}
